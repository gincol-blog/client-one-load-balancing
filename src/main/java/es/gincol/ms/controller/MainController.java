package es.gincol.ms.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.gincol.ms.utils.Utils;

@RestController
public class MainController {

	private static Logger log = LoggerFactory.getLogger(MainController.class);
	
	@Value("${server.port}")
	String puerto;
	@Value("${spring.application.name}")
	private String appName;

	@GetMapping("/saludo")
	public String saludo(HttpServletRequest request) {
		log.info("Acceso /saludo");
		
		List<String> greetings = Arrays.asList("Hola", "Saludos", "Saludos cordiales");
		Random rand = new Random();

		int randomNum = rand.nextInt(greetings.size());
		String retorno = greetings.get(randomNum);
		return retorno + ", desde puerto " + puerto + ", en fecha " + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) + " !!!" + "<h3>app: '"
		+ appName + "'</h3>" + "<p>host name / ip: <b>" + Utils.getIp() + "</b></p>" + "<p>Session: <b>"
		+ request.getSession().getId() + "</b></p>";
	}

	@RequestMapping(value = "/")
	public String home() {
		log.info("Acceso /");
		return "Hola!";
	}
	
}
