def imagenApp = "client-one-load-balancing"
def host = "localhost:5000"
def customImagenApp = ""
def dockerFolder = "docker"
def cloudFolder = "cloud"

pipeline {

    agent any
	
	tools { 
        maven "mvn350" 
        jdk "jdk8"
    }

	stages {
	
		stage ("Initialize") {
            steps {
				echo "PATH = ${PATH}"
				echo "M2_HOME = ${M2_HOME}"
				echo "Inicializamos jenkins para usar docker"
				sh "eval \$(minikube docker-env)"
            }
        }
	    
	    stage ("Build") {
            steps {
            	echo "Inicio maven"
                sh "mvn package"
                echo "Fin maven" 
            }
        }
        
        stage("Push image to Registry") {
        	steps {
				script {
			        docker.withRegistry("http://${host}") {
			        	echo "Construimos ${imagenApp}"
			        	customImagenApp = docker.build("${imagenApp}", "-f ${dockerFolder}/Dockerfile --no-cache .")
			        	echo "Subimos ${imagenApp} al registro privado"
			            customImagenApp.push()
			        }
		        }
		        
	        }
	    }
	    
	    stage("Deploy Kubernetes") {
	    	steps {
	    		script {
		    		try {
		    			echo "Se aplica ingress para el dominio 'gincol.blog.com'"
		    			sh "kubectl apply -f ${cloudFolder}/Ingress.yaml"
		    		} catch (exc) {
		    			echo "Ingress error"
		    		}
		    		
		    		try {
		    			echo "Creamos deployment y servicio ${imagenApp}"
		    			sh "kubectl create -f ${cloudFolder}/App.yaml"
		    		} catch (exc) {
		    			echo "Ya existe, lo recreamos"
						sh "kubectl replace --force --cascade=true -f ${cloudFolder}/App.yaml"
		    		}
		    	}
		    }
	    }
	    
	    stage("Delete images") {
	    	steps {
	    		script {
	    			try {
		    			echo "Borramos imagen ${imagenApp}"
		    			sh "docker rmi ${imagenApp}"
	    			} catch (exc) {
		    			echo "no se puede borrar imagen ${imagenApp}"
		    		}
	    			try {
		    			echo "Borramos imagen ${host}/${imagenApp}"
		    			sh "docker rmi ${host}/${imagenApp}"
	    			} catch (exc) {
		    			echo "no se puede borrar imagen ${host}/${imagenApp}"
		    		}
		    		
		    		try {
		    			echo "Borramos imagenes obsoletas"
		    			sh "docker rmi \$(docker images --quiet --filter 'dangling=true')"
		    		} catch (exc) {
		    			echo "No se pueden borrar imagenes obsoletas"
		    		}
	    		}
	    	}
	    }
	    	
    }
        
	post {
        always {
			echo "Fin Proceso"
        }
        success {
            echo "Fin success";
        }
        unstable {
            echo "Fin unstable";
        }
        failure {
            echo "Fin failure";
        }
    }
}